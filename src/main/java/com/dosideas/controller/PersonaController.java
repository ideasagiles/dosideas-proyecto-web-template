/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosideas.controller;

import com.dosideas.domain.Persona;
import com.dosideas.repository.PersonaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ldeseta
 */
@Controller
public class PersonaController {

    @Autowired
    private PersonaRepository personaRepository;

    @RequestMapping("/personas")
    public @ResponseBody List<Persona> findAll() {
        return personaRepository.findAll();
    }

}
