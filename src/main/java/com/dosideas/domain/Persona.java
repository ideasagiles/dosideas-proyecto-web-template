/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosideas.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author ldeseta
 */
@Entity
public class Persona {

    @Id
    private Integer id;
    private String nombre;
    private String serie;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

}
