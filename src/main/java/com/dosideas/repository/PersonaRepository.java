/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosideas.repository;

import com.dosideas.domain.Persona;
import java.util.List;
import org.springframework.data.repository.Repository;

/**
 *
 * @author ldeseta
 */
public interface PersonaRepository extends Repository<Persona, Integer> {

    List<Persona> findAll();
}
