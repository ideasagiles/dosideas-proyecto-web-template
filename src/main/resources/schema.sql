drop table if exists persona;

create table persona (
    id bigint identity primary key,
    nombre varchar(100) not null,
    serie varchar(100) not null
);

insert into persona (nombre, serie) values ('Dib', 'Invader Zim');
insert into persona (nombre, serie) values ('Gaz', 'Invader Zim');
insert into persona (nombre, serie) values ('Gir', 'Invader Zim');
insert into persona (nombre, serie) values ('Sheldon', 'The Big Bang Theory');
insert into persona (nombre, serie) values ('Leonard', 'The Big Bang Theory');
insert into persona (nombre, serie) values ('Penny', 'The Big Bang Theory');
insert into persona (nombre, serie) values ('Jack Bauer', '24');
insert into persona (nombre, serie) values ('Fox Moulder', 'The X Files');
insert into persona (nombre, serie) values ('John Snow', 'Game of Thrones');
insert into persona (nombre, serie) values ('Jean-Luc Picard', 'Star Trek: The next generation');
insert into persona (nombre, serie) values ('Illya Kuryakin', 'El agente de CIPOL');
insert into persona (nombre, serie) values ('Napoleon Solo', 'El agente de CIPOL');
insert into persona (nombre, serie) values ('Maxwell Smart', 'Get Smart');
insert into persona (nombre, serie) values ('Apu Nahasapeemapetilon', 'Los Simpsons');
insert into persona (nombre, serie) values ('David Banner', 'El increible Hulk');
insert into persona (nombre, serie) values ('J.R. Ewing', 'Dinastia');
insert into persona (nombre, serie) values ('Gregory House', 'House');
