<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>

<h1>Bienvenido a la demo</h1>
<ul>
    <li><a href="<c:url value="/persona/listado.html"/>">Listado de personas</a></li>
</ul>

<jwr:script src="/js/view/bienvenido.js" />
