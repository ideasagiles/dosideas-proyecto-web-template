<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>

<h1>Listado de personas</h1>
<input id="personasButton" type="button" value="Refrescar" />

<ul id="personas"></ul>


<!--********************************************************************
                             TEMPLATES
*********************************************************************-->
<script id="personasTemplate" type="text/x-jsrender">
    <li>{{:nombre}}, aparece en {{:serie}}</li>
</script>

<jwr:script src="/js/view/persona/listado.js" />
