/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

dosideas.service.persona = (function() {

    function findAll(onSuccessCallback, onErrorCallback) {
        dosideas.service.getJSON(dosideas.service.serviceBaseURI + "/personas", onSuccessCallback, onErrorCallback);
    }

    function guardar(persona, onSuccessCallback, onErrorCallback) {
        dosideas.service.postJSON(dosideas.service.serviceBaseURI + "/personas", persona, onSuccessCallback, onErrorCallback);
    }

    return {
        findAll: findAll,
        guardar: guardar
    }
})();