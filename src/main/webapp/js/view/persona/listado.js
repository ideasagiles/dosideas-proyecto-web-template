/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/** Este archivo es propio para la pÃ¡gina /WEB-INF/jsp/bienvenido.jsp */

/** Representa el elemento que busca personas en la pÃ¡gina. Este objeto se
 *  encarga de la inicializaciÃ³n del componente (setup) y de dibujar los
 *  datos en pantalla.
 */
var buscadorPersonas = {
    setup : function () {
        $(document).on("click", "#personasButton", function () {
            dosideas.service.persona.findAll(buscadorPersonas.render);
        });
    },
    render : function (personas) {
        $("#personas").html($("#personasTemplate").render(personas));
    }
}

/** InicializaciÃ³n de la pÃ¡gina. */
$(document).ready(function () {
    buscadorPersonas.setup();
});
