/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosideas.controller;

import javax.sql.DataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;



/**
 *
 * @author parivero
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@WebAppConfiguration
public class PersonaControllerTest {

    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private DataSource dataSource;
    
    private MockMvc mockMvc;
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(wac).build();
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void findAll_existenPersonas_retornaListaDePersonasenFormatoJson() throws Exception {
        int filas = JdbcTestUtils.countRowsInTable(jdbcTemplate, "persona");
        this.mockMvc.perform(get("/personas"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(hasSize(filas)));
    }
}