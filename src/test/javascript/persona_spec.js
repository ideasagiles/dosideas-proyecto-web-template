describe('persona test', function() {
    var callbackOk = jasmine.createSpy();
    var callbackError = jasmine.createSpy();
    var persona;
    
    it('post de persona retorna Ok', function() {
        var responseData = 'Ok';
    
        registerFakeAjax({
            url: '/persona',
            type: 'post',
            successData: responseData
        });
        dosideas.service.persona.guardar(persona,callbackOk,callbackError);
        expect(callbackOk).toHaveBeenCalledWith(responseData);
    });

    it('post de persona retorna error', function() {
        var esperado = {
            responseText : 'fallo'
        };

        registerFakeAjax({
            url: '/persona',
            type: 'post',
            errorMessage: 'fallo'
        });
        dosideas.service.persona.guardar(persona,callbackOk,callbackError);
        expect(callbackError).toHaveBeenCalledWith(esperado);
    });
});