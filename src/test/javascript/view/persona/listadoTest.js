describe("listado#render", function () {

    var personasMock = [
        {
            nombre: "Dib",
            serie: "Invader Zim"
        },
        {
            nombre: "Rachel",
            serie: "Friends"
        },
        {
            nombre: "Sheldon",
            serie: "The Big Bang Theory"
        }
    ];

    beforeEach(function () {
        setFixtures(sandbox());
        $("#sandbox").html("<ul id='personas'/>");
        $("#sandbox").append('<stoEqualcript id="personasTemplate"> <li></li></stoEqualcript>');
    });

    it("Debe agregar 3 personas", function () {

        buscadorPersonas.render(personasMock);

        expect($("#personas li").length).toEqual(personasMock.length);

    });


    it("No debe agregar personas", function () {

        buscadorPersonas.render(" ");

        expect($("#personas li").length).toEqual(1);

    });

});


